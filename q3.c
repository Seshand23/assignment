#include<stdio.h>

int main()
{
	int temp, num1, num2;

	printf("Enter number 1 : ");
	scanf("%d", &num1);

	printf("Enter number 2 : ");
	scanf("%d", &num2);

	printf("Before Swapping \n Number 1 is %d \n Number 2 is %d \n\n", num1, num2);

	temp = num1;
	num1 = num2;
	num2 = temp;

	printf("After Swapping \n Number 1 is %d \n Number 2 is %d \n", num1, num2);

	return 0;
}
