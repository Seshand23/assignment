#include<stdio.h>

int main()
{
	double r;

	printf("Enter the radius value : ");
	scanf("%lf",&r);

	printf("Area of the disk with radius %.4f is %.4f\n", r , 22/7.0 * r * r);

	return 0;	
}
